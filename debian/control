Source: libarray-unique-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Joenio Marques da Costa <joenio@joenio.me>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl
Build-Depends-Indep: perl
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libarray-unique-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libarray-unique-perl.git
Homepage: https://metacpan.org/release/Array-Unique
Rules-Requires-Root: no

Package: libarray-unique-perl
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         ${perl:Depends},
         libtie-ixhash-perl
Description: Tie-able array that allows only unique values
 Array::Unique lets you create an array which will allow only one occurrence of
 any value. In other words, no matter how many times you put in 42, it will keep
 only the first occurrence and the rest will be dropped.
 .
 Uniqueness is checked with the 'eq' operator so among other things it is case
 sensitive. As a side effect, the module does not allow undef as a value in the
 array.
